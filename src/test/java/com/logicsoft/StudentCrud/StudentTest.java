package com.logicsoft.StudentCrud;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import  com.logicsoft.StudentCrud.model.Student;
import  com.logicsoft.StudentCrud.repository.StudentRepository;
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
class StudentTest {
@Autowired
private StudentRepository studentrepo;
@Test
@Rollback(false)
public void registerStudent() {
	Student student=new Student(1,"prashant","Delhi",45);
	Student save=studentrepo.save(student);
	assertNotNull(save);
  }
@Test
public void singleStudentDetails() {
	Integer rollnumber=7;
	 Student student = studentrepo.findById(rollnumber).orElse(null);
	 assertThat(student.getRollNumber()).isEqualByComparingTo(rollnumber);
}

@Test
public void studentdelete() {
	 Integer rollnumber=7;
	 boolean isexitancebeforeDelete = studentrepo.findById(rollnumber).isPresent();
	   studentrepo.deleteById(rollnumber);
	   System.out.print("if existance"+isexitancebeforeDelete);
	 boolean notexitanceafterDelete = studentrepo.findById(rollnumber).isPresent();
		assertTrue(isexitancebeforeDelete);
		System.out.print("if  not existance"+notexitanceafterDelete);
		assertFalse(notexitanceafterDelete);
}
//
@Test
public void testUpdate() {
      Integer rollNumber=1;
      Student student=new Student(rollNumber,"Abhishek","Delhi",45);
      Student save=studentrepo.save(student);
	  Student updatedstudent=studentrepo.findById(rollNumber).get();
	  System.out.println(updatedstudent.getName());
	 assertThat(updatedstudent.getRollNumber()).isEqualTo(rollNumber);
}
@Test
public void testlistStudent() {
	List<Student>student=(List<Student>) studentrepo.findAll();
	assertThat(student.size()).isGreaterThan(0);
	
}

}

