package com.logicsoft.StudentCrud.repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.logicsoft.StudentCrud.model.Student;
@Repository
public interface StudentRepository extends CrudRepository<Student,Integer>{

	

}
