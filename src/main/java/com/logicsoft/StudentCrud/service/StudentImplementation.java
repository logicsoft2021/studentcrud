package com.logicsoft.StudentCrud.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicsoft.StudentCrud.model.Student;
import com.logicsoft.StudentCrud.repository.StudentRepository;
@Service
public class StudentImplementation implements StudentServices{
	@Autowired
	private StudentRepository studentRepository;
	@Override
	public Student registerStudent(Student student) {
		// TODO Auto-generated method stub
	    return studentRepository.save(student);
	}
//	@Override
//	public List<Student> getStudents() {
//		return (List<Student>) studentRepository.findAll();
//	}	
	@Override
	public List<Student> getStudents() {
		return (List<Student>) studentRepository.findAll();
	}
	
	@Override
	public void deleteStudent(Integer id) {
		studentRepository.deleteById(id);	
	}
	@Override
	public Student updateStudent(Student student) {
		Integer rollNumber=student.getRollNumber();
		Student std= studentRepository.findById(rollNumber).get();
		std.setName(student.getName());
		std.setAddress(student.getAddress());
		std.setPercentages(student.getPercentages());
		return studentRepository.save(student);
	}
	@Override
	public Student getStudentById(Integer id) {
		return studentRepository.findById(id).orElse(null);
	}
	
}
