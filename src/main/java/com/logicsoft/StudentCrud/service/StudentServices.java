package com.logicsoft.StudentCrud.service;
import java.util.List;

import com.logicsoft.StudentCrud.model.Student;
public interface StudentServices {
	 abstract Student registerStudent(Student student);
	 abstract List<Student> getStudents();
	 abstract void deleteStudent(Integer id);
	 abstract Student updateStudent(Student student);
	abstract Student getStudentById(Integer id);
}
