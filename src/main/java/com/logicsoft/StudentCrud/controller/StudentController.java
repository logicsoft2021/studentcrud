package com.logicsoft.StudentCrud.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logicsoft.StudentCrud.model.Student;
import com.logicsoft.StudentCrud.service.StudentServices;
@RestController
public class StudentController{	
	//private StudentService studentservice;
	@Autowired
	private StudentServices studentservice;
	@PostMapping("/registerStudent")
	public Student registerStudent(@RequestBody Student student) {
		return studentservice.registerStudent(student);
	}
//	@GetMapping("/getStudents")
//	public List <Student> getStudent(){
//		return studentservice.getStudents();
//	} 
//	@GetMapping("/getStudentById")
//	public List <Student> getStudentById(){
//		return studentservice.getStudents();
//	}	
	@GetMapping("/getStudents/{id}")
	public Student getStudentById(@PathVariable Integer id) {
		return studentservice.getStudentById(id);
	}
	@DeleteMapping("/deleteStudent")
	public void deleteStudent(@RequestParam Integer id) {
		studentservice.deleteStudent(id);
	}
	@PutMapping("/updateStudent")
	public Student updateStudent(@RequestBody Student student) {
		return studentservice.updateStudent(student);
		
	}
} 
