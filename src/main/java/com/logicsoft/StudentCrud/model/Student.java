package com.logicsoft.StudentCrud.model;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity(name="student")
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@RequiredArgsConstructor
public class Student implements Serializable {
	private static final long serialVersionUID = 1L;
    protected static final String PK = "rollNumber";
    @Id
    @Column(name="roll_number", unique=true, nullable=false, precision=10)
    private int rollNumber;
    @Column(length=255)
    private String address;
    @Column(length=255)
    private String name;
    @Column(precision=17, scale=17)
    private double percentages;
	 public Map<String, Object> getPrimaryKey() { Map<String, Object> ret = new
	 LinkedHashMap<String, Object>(6); ret.put("rollNumber",
	 Integer.valueOf(getRollNumber())); return ret;
	  }
}
